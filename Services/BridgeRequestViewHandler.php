<?php

namespace ThinkCreative\BridgeBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use eZ\Publish\Core\MVC\Symfony\View\ContentViewInterface;
use ThinkCreative\CoreBundle\Services\RequestViewHandler;

class BridgeRequestViewHandler extends RequestViewHandler
{

    public function __construct(array $project = array()) {
        parent::__construct(
            array_replace_recursive(
                $project,
                array(
                    'is_content' => false,
                    'is_main_location' => true,
                )
            )
        );
    }

    public function isContentRequest() {
        if($this->Controller) {
            return
                $this->Controller->Class == 'eZ\Publish\Core\MVC\Symfony\Controller\Content\ViewController' &&
                ($this->Controller->Action == 'viewLocation' || $this->Controller->Action == 'viewContent')
            ;
        }
        return false;
    }

    public function processContentView(ContainerInterface $container, ContentViewInterface $content_view) {
        $ContentViewParameters = $content_view->getParameters();
        $LocationService = $container->get('ezpublish.api.repository')->getLocationService();

        $Content = $ContentViewParameters['content'];
        $MainLocation = $Location = $ContentViewParameters['location'];

        if($Location->id != $Content->contentInfo->mainLocationId) {
            $this->Parameters->set(
                'is_main_location', false
            );

            $content_view->addParameters(
                array(
                    'main_location' => $MainLocation = $LocationService->loadLocation($Content->contentInfo->mainLocationId),
                )
            );
        }

        $HomepageLocationID = 2;
        if(
            $container->has('ezpublish.siteaccess')
        ) {
            $SiteAccess = $container->get('ezpublish.siteaccess');
            if(
                $container->hasParameter('ezsettings.' . $SiteAccess->name . '.content.tree_root.location_id')
            ) {
                $HomepageLocationID = (int) $container->getParameter(
                    "ezsettings.$SiteAccess->name.content.tree_root.location_id"
                );
            }
        }

        $ContentBoxService = $container->get('thinkcreative.contentbox');
        if($Location !== null) {
            $ContentBoxService->loadContentBoxList(
                $Location, $HomepageLocationID
            );
        }

        $this->Parameters->add(
            array(
                'name' => $Content->contentInfo->name,
                'canonical' => $container->get('router')->generate(
                    $MainLocation, array(), UrlGeneratorInterface::ABSOLUTE_PATH
                ),
                'is_homepage' => $Location->id === $HomepageLocationID,
                'is_content' => true,
                'has_content_box_list' => $ContentBoxService->hasContentBoxList(),
            )
        );
    }

}

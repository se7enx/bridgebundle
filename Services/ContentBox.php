<?php

namespace ThinkCreative\BridgeBundle\Services;

use eZ\Publish\API\Repository\Values\Content\Location;
use eZ\Publish\API\Repository\Values\Content\Search\SearchResult;
use Aw\Ezp\FetchBundle\Fetch\Fetcher;

class ContentBox
{

    protected $SearchHandler;
    protected $ContentBoxList;

    public function __construct(Fetcher $aw_ezp_fetch) {
        $this->SearchHandler = $aw_ezp_fetch;
        $this->ContentBoxList = array();
    }

    public function getContentBoxList() {
        return $this->ContentBoxList;
    }

    public function hasContentBoxList() {
        return (bool) count($this->ContentBoxList);
    }

    public function loadContentBoxList(Location $location, $location_limit = 0) {
        $ContentBoxList = array();

        $Continue = true;
        $LocationPath = array_reverse(
            $location->attribute('path')
        );

        do {
            $PathLocationID = (int) current($LocationPath);

            $ContentBoxFilter = array(
                'AND' => array(
                    array(
                        'parent_location_id' => array(
                            'EQ' => $PathLocationID
                        ),
                    ),
                    array(
                        'visibility' => array(
                            'EQ' => true
                        ),
                    ),
                    array(
                        'content_type_identifier' => array(
                            'EQ' => 'content_box'
                        ),
                    ),
                )
            );

            if($PathLocationID !== $location->id) {
                $ContentBoxFilter['AND'][] = array(
                    'field.persistent' => array(
                        'EQ' => true
                    )
                );
            }

            $SearchResults = $this->SearchHandler->fetch(
                array(
                    'filter' => $ContentBoxFilter,
                )
            );

            if(
                $SearchResults && $SearchResults instanceof SearchResult
            ) {
                foreach(
                    $SearchResults->searchHits as $Item
                ) {
                    $ContentBoxList[] = $Item->valueObject;
                }
            }

            if(
                $PathLocationID === $location_limit || next($LocationPath) === false
            ) {
                $Continue = false;
            }

        } while($Continue);

        $this->ContentBoxList = $ContentBoxList;
    }

}

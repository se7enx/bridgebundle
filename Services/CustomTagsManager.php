<?php

namespace ThinkCreative\BridgeBundle\Services;

use Symfony\Component\HttpKernel\Fragment\FragmentHandler;
use Symfony\Component\HttpKernel\Controller\ControllerReference;
use ThinkCreative\BridgeBundle\Classes\CustomTag;
use ThinkCreative\BridgeBundle\Classes\CustomTagDefinition;
use ThinkCreative\BridgeBundle\DependencyInjection\CustomTagsHandlerInterface;

class CustomTagsManager
{

    protected $Defaults;
    protected $FragmentHandler;
    protected $TwigEnvironment;
    protected $CustomTagsHandlers;
    protected $RegisteredItems;

    public function __construct(FragmentHandler $handler, array $defaults) {
        $this->Defaults = $defaults;
        $this->FragmentHandler = $handler;
        $this->CustomTagsHandlers = array();
        $this->RegisteredItems = array();
    }

    public function addCustomTagsHandler(CustomTagsHandlerInterface $handler) {
        $this->CustomTagsHandlers[] = $handler;
    }

    public function createCustomTag($name, array $attributes = array(), $content = '') {
        $Definition = $this->getRegisteredItem($name);

        $CustomTag = new CustomTag($name);
        $CustomTag->setContent($content);

        foreach(
            $Definition->getAttributeList() as $Name
        ) {
            $CustomTag->setAttribute(
                $Name, isset($attributes[$Name]) ? $attributes[$Name] : $Definition->getAttributeValue($Name)
            );
        }

        return $CustomTag;
    }

    public function getRegisteredItem($name) {

        // Hack to fix issue where the link attribute doesn't work for
        // custom tags with an underscore in the name
        // This will allow custom tags to work that were previously embedded
        // with an underscore.
        // @see https://jira.ez.no/browse/EZP-21590
        $name = str_replace("_", "-", $name);

        return $this->RegisteredItems[$name];
    }

    public function getRegisteredItems() {
        return $this->RegisteredItems;
    }

    public function hasRegisteredItem($name) {

        // See note in CustomTagsManager::getRegisteredItem
        $name = str_replace("_", "-", $name);

        return isset(
            $this->RegisteredItems[$name]
        );
    }

    public function loadCustomTagsHandlers() {
        foreach($this->CustomTagsHandlers as $CustomTagsHandler) {
            $CustomTagsHandler->registerCustomTagList($this);
        }
    }

    public function register($name, $options = array()) {
        if(
            $this->hasRegisteredItem($name)
        ) {
            $CustomTag = $this->getRegisteredItem($name);
            return;
        }

        if(
            isset($options['attributes'])
        ) {
            foreach(
                $options['attributes'] as $Name => $Item
            ) {
                if($Item) {
                    $options['attributes'][$Name] = array_replace(
                        $this->Defaults['attribute_options'], $Item
                    );
                }
            }
        }

        $options = array_replace(
            $this->Defaults['tag_options'], $options
        );

        $this->setRegisteredItem(
            new CustomTagDefinition($name, $options)
        );
    }

    public function renderCustomTag(CustomTag $customtag) {
        $Definition = $this->getRegisteredItem(
            $CustomTagName = $customtag->getName()
        );

        $ControllerParameters = array(
            'template' => $Definition->Template,
            'variables' => array_merge(
                $customtag->getAttributes(),
                array(
                    '_name' => $CustomTagName,
                    '_attributes' => $customtag->getAttributes(),
                    'content' => $customtag->getContent(),
                )
            ),
        );

        if($Definition->Controller === false) {
            return $this->TwigEnvironment->render(
                $ControllerParameters['template'], $ControllerParameters['variables']
            );
        }

        return $this->FragmentHandler->render(
            new ControllerReference( $Definition->Controller, $ControllerParameters ), $Definition->Strategy, array()
        );
    }

    public function setTwigEnvironment(\Twig_Environment $twig) {
        $this->TwigEnvironment = $twig;
    }

    public function setRegisteredItem(CustomTagDefinition $item) {
        $this->RegisteredItems[$item->Name] = $item;
    }

}

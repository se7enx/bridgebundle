<?php

namespace ThinkCreative\BridgeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ContentBoxController extends Controller
{

    public function indexAction($template = 'ThinkCreativeBridgeBundle:parts/contentbox:list.html.twig') {
        $ContentBoxService = $this->container->get('thinkcreative.contentbox');
        return $this->render(
            $template, array(
                'content_box_list' => $ContentBoxService->getContentBoxList()
            )
        );
    }

}

<?php

namespace ThinkCreative\BridgeBundle\Classes;

class CustomTag
{

    protected $Name;
    protected $Attributes;
    protected $Content;

    public function __construct($name, array $attributes = array(), $content = '') {
        $this->Name = $name;
        $this->Attributes = $attributes;
        $this->Content = $content;
    }

    public function getAttributes() {
        return $this->Attributes;
    }

    public function getContent() {
        return $this->Content;
    }

    public function getName() {
        return $this->Name;
    }

    public function setAttribute($name, $value) {
        $this->Attributes[$name] = $value;
    }

    public function setContent($content) {
        $this->Content = $content;
    }

}

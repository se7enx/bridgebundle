<?php

namespace ThinkCreative\BridgeBundle\Classes;

class CustomTagDefinition
{

    public $Name;
    public $Attributes;
    public $isInline;
    public $Controller;
    public $Template;
    public $Strategy;
    public $Access;

    public function __construct($name, array $options) {
        $this->Name = $name;
        $this->Attributes = (
            isset($options['attributes']) ? $options['attributes'] : array()
        );
        $this->isInline = (
            isset($options['is_inline']) ? $options['is_inline'] : false
        );

        $this->Controller = $options['controller'];
        $this->Template = $options['template'];
        $this->Strategy = $options['strategy'];
        $this->Access = $options['access'];
    }

    public function getAttributeList() {
        return array_keys($this->Attributes);
    }

    public function getAttributeValue($name) {
        if(
            isset($this->Attributes[$name]) && is_array($this->Attributes[$name]) && isset($this->Attributes[$name]['default'])
        ) {
            return $this->Attributes[$name]['default'];
        }
        return '';
    }

}

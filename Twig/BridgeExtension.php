<?php

namespace ThinkCreative\BridgeBundle\Twig;

use eZ\Publish\API\Repository\Repository as eZPublishRepository;
use ThinkCreative\BridgeBundle\Services\MenubarBridge;

class BridgeExtension extends \Twig_Extension
{

    protected $eZPublishRepository;
    protected $MenubarBridgeService;

    public function __construct(eZPublishRepository $ezpublish_repository, MenubarBridge $menubar_bridge) {
        $this->eZPublishRepository = $ezpublish_repository;
        $this->MenubarBridgeService = $menubar_bridge;
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter(
                'menubar_items', array($this, 'generateMenubarItems',)
            )
        );
    }

    public function generateMenubarItems(array $list, $is_location = true) {
        foreach($list as $Key => $ItemID) {

            $Item = $this->eZPublishRepository->getLocationService()->loadLocation(
                $is_location ? $ItemID : $this->eZPublishRepository->getContentService()->loadContentInfo($ItemID)->mainLocationId
            );

            $ItemParameters = array();

            $ContentService = $this->eZPublishRepository->getContentService();
            $ContentTypeService = $this->eZPublishRepository->getContentTypeService();
            $ContentType = $ContentTypeService->loadContentType(
                $Item->contentInfo->contentTypeId
            );

            if ($ContentType->identifier == 'link') {
                $Content = $ContentService->loadContent($Item->contentInfo->id);
                $ItemParameters['new_window'] = $Content->getFieldValue('open_in_new_window')->bool;
            }

            $list[$Key] = $this->MenubarBridgeService->getLocationData(
                $Item, $ItemParameters
            );
        }
        return $list;
    }

    public function getName() {
        return 'bridge';
    }

}

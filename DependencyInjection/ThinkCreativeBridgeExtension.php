<?php

namespace ThinkCreative\BridgeBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class ThinkCreativeBridgeExtension extends Extension
{

    public function load(array $configurations, ContainerBuilder $container) {
        $YamlFileLoader = new YamlFileLoader(
            $container, new FileLocator(__DIR__ . '/../Resources/config')
        );
        $YamlFileLoader->load('parameters.yml');
        $YamlFileLoader->load('services.yml');
    }

    public function getAlias() {
        return 'think_creative_bridge';
    }

}

<?php

namespace ThinkCreative\BridgeBundle\DependencyInjection;

use ThinkCreative\BridgeBundle\Services\CustomTagsManager;

interface CustomTagsHandlerInterface
{

    public function registerCustomTagList(CustomTagsManager $customtags_manager);

}

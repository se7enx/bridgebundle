<?php

namespace ThinkCreative\BridgeBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class BridgeCompilerPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container) {

        if(
            $container->hasDefinition('thinkcreative.customtagsmanager')
        ) {
            $CustomTagsManager = $container->getDefinition('thinkcreative.customtagsmanager');
            $CustomTagsServices = $container->findTaggedServiceIds('thinkcreative.customtags');

            foreach ($CustomTagsServices as $ID => $Attributes) {
                $CustomTagsManager->addMethodCall(
                    'addCustomTagsHandler', array( new Reference($ID) )
                );
            }
            $CustomTagsManager->addMethodCall('loadCustomTagsHandlers');
        }

    }

}
